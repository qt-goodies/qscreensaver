import qbs

StaticLibrary {
    name: "QScreenSaver"

    property string srcDir: "src/ScreenSaver/"

    cpp.cxxLanguageVersion: "c++17"

    files: [
        srcDir + "inhibitor.h",
    ]

    Group {
        id: fdoGroup
        condition: qbs.targetPlatform === "linux"
        files: srcDir + "inhibitor_fdo.cpp"
    }

    Group {
        condition: qbs.targetOS.contains("darwin")
        files: srcDir + "inhibitor_mac.cpp"
    }

    Group {
        condition: qbs.targetOS.contains("windows")
        files: srcDir + "inhibitor_windows.cpp"
    }

    Group {
        // Update this to exclude all of the above
        condition: qbs.targetPlatform != "linux"
            && !qbs.targetOS.contains("darwin")
            && !qbs.targetOS.contains("windows")
        files: srcDir + "inhibitor_dummy.cpp"
    }

    Depends { name: "cpp" }
    Depends { name: "Qt.core" }
    Depends {
        condition: fdoGroup.condition
        name: "Qt.dbus"
    }

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: [exportingProduct.sourceDirectory + '/src']
        cpp.libraryPaths: [exportingProduct.buildDirectory]
    }
}
