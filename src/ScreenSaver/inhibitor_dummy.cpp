/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QScreenSaver.
 *
 * QScreenSaver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QScreenSaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "inhibitor.h"

#include <QDebug>

using namespace it::mardy::ScreenSaver;

namespace it::mardy::ScreenSaver {

class InhibitorPrivate
{
};

} // namespace

Inhibitor::Inhibitor(QObject *parent):
    QObject(parent)
{
    qWarning() << "ScreenSaver inhibitor not implemented for this platform";
}

Inhibitor::~Inhibitor() = default;

void Inhibitor::setActive(bool)
{
}

bool Inhibitor::isActive() const
{
    return false;
}
