/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QScreenSaver.
 *
 * QScreenSaver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QScreenSaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IT_MARDY_SCREENSAVER_INHIBITOR_H
#define IT_MARDY_SCREENSAVER_INHIBITOR_H

#include <QObject>
#include <QScopedPointer>

namespace it {
namespace mardy {
namespace ScreenSaver {

class InhibitorPrivate;
class Inhibitor: public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool active READ isActive WRITE setActive
               NOTIFY isActiveChanged)

public:
    Inhibitor(QObject *parent = nullptr);
    virtual ~Inhibitor();

    void setActive(bool active);
    bool isActive() const;

Q_SIGNALS:
    void isActiveChanged();

private:
    Q_DECLARE_PRIVATE(Inhibitor)
    QScopedPointer<InhibitorPrivate> d_ptr;
};

}}} // namespace

#endif // IT_MARDY_SCREENSAVER_INHIBITOR_H
