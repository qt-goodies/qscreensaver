/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QScreenSaver.
 *
 * QScreenSaver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QScreenSaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "inhibitor.h"

#include <QCoreApplication>
#include <QDBusConnection>
#include <QDBusMessage>
#include <QDBusPendingCall>
#include <QDBusPendingCallWatcher>
#include <QDBusPendingReply>
#include <QDebug>

using namespace it::mardy::ScreenSaver;

namespace it {
namespace mardy {
namespace ScreenSaver {

class InhibitorPrivate
{
    Q_DECLARE_PUBLIC(Inhibitor)

public:
    InhibitorPrivate(Inhibitor *q);

    void inhibit();
    void unInhibit();

private:
    QDBusConnection m_conn;
    QDBusMessage m_msgInhibit;
    QDBusMessage m_msgUnInhibit;
    bool m_isActive;
    quint32 m_cookie;
    Inhibitor *q_ptr;
};

}}} // namespace

InhibitorPrivate::InhibitorPrivate(Inhibitor *q):
    m_conn(QDBusConnection::sessionBus()),
    m_isActive(false),
    m_cookie(0),
    q_ptr(q)
{
    const QString serviceName(QStringLiteral("org.freedesktop.ScreenSaver"));
    const QString objectPath(QStringLiteral("/org/freedesktop/ScreenSaver"));
    const QString interfaceName = serviceName;

    m_msgInhibit = QDBusMessage::createMethodCall(
        serviceName, objectPath, interfaceName, QStringLiteral("Inhibit"));
    m_msgInhibit.setArguments({
        QCoreApplication::applicationName(),
        "QScreenSaver"
    });

    m_msgUnInhibit = QDBusMessage::createMethodCall(
        serviceName, objectPath, interfaceName, QStringLiteral("UnInhibit"));
}

void InhibitorPrivate::inhibit()
{
    Q_Q(Inhibitor);

    if (m_isActive) {
        qWarning() << "Screensaver inhibitor already active!";
        return;
    }

    QDBusPendingCallWatcher *call =
        new QDBusPendingCallWatcher(m_conn.asyncCall(m_msgInhibit), q);
    QObject::connect(call, &QDBusPendingCallWatcher::finished,
                     q, [this, q](QDBusPendingCallWatcher *call) {
        call->deleteLater();
        QDBusPendingReply<quint32> reply = *call;
        if (reply.isError()) {
            qWarning() << "Failed to inhibit screensaver:" << reply.error();
            return;
        }

        m_isActive = true;
        m_cookie = reply.value();
        Q_EMIT q->isActiveChanged();
    });
}

void InhibitorPrivate::unInhibit()
{
    Q_Q(Inhibitor);

    if (!m_isActive) {
        return;
    }

    m_msgUnInhibit.setArguments({ m_cookie });
    QDBusPendingCallWatcher *call =
        new QDBusPendingCallWatcher(m_conn.asyncCall(m_msgUnInhibit), q);
    QObject::connect(call, &QDBusPendingCallWatcher::finished,
                     q, [this, q](QDBusPendingCallWatcher *call) {
        call->deleteLater();
        QDBusPendingReply<void> reply = *call;
        if (reply.isError()) {
            qWarning() << "Failed to uninhibit screensaver:" << reply.error();
            return;
        }

        m_isActive = false;
        m_cookie = 0;
        Q_EMIT q->isActiveChanged();
    });
}

Inhibitor::Inhibitor(QObject *parent):
    QObject(parent),
    d_ptr(new InhibitorPrivate(this))
{
}

Inhibitor::~Inhibitor() = default;

void Inhibitor::setActive(bool active)
{
    Q_D(Inhibitor);
    if (active) {
        d->inhibit();
    } else {
        d->unInhibit();
    }
}

bool Inhibitor::isActive() const
{
    Q_D(const Inhibitor);
    return d->m_isActive;
}
