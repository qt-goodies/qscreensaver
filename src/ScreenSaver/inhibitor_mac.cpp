/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QScreenSaver.
 *
 * QScreenSaver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QScreenSaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "inhibitor.h"

#include <QDebug>

#include <IOKit/pwr_mgt/IOPMLib.h>

using namespace it::mardy::ScreenSaver;

namespace it::mardy::ScreenSaver {

class InhibitorPrivate
{
private:
    friend class Inhibitor;
	IOPMAssertionID m_assertionId = 0;
};

} // namespace

Inhibitor::Inhibitor(QObject *parent):
    QObject(parent),
    d_ptr(new InhibitorPrivate())
{
}

Inhibitor::~Inhibitor() = default;

void Inhibitor::setActive(bool active)
{
    Q_D(Inhibitor);
    if (active == isActive()) return;

    if (active) {
        CFStringRef assertionType = kIOPMAssertionTypeNoDisplaySleep;
        IOReturn r = IOPMAssertionCreateWithName(assertionType,
                                    kIOPMAssertionLevelOn,
                                    CFSTR("Playing media"),
                                    &d->m_assertionId);
        if (r != kIOReturnSuccess) {
            qWarning() << "Could not inhibit screensaver";
            return;
        }
    } else {
        IOReturn r = IOPMAssertionRelease(d->m_assertionId);
        if (r != kIOReturnSuccess) {
            qWarning() << "Could not uninhibit screensaver";
            return;
        }
        d->m_assertionId = 0;
    }

    Q_EMIT isActiveChanged();
}

bool Inhibitor::isActive() const
{
    Q_D(const Inhibitor);
    return d->m_assertionId != 0;
}
