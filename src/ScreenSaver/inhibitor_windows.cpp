/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QScreenSaver.
 *
 * QScreenSaver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QScreenSaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "inhibitor.h"

#include <QDebug>

#include <windows.h>

using namespace it::mardy::ScreenSaver;

namespace it::mardy::ScreenSaver {

class InhibitorPrivate
{
private:
    friend class Inhibitor;
    bool m_isActive = false;
};

} // namespace

Inhibitor::Inhibitor(QObject *parent):
    QObject(parent),
    d_ptr(new InhibitorPrivate())
{
}

Inhibitor::~Inhibitor() = default;

void Inhibitor::setActive(bool active)
{
    Q_D(Inhibitor);
    if (active == d->m_isActive) return;

    EXECUTION_STATE state = active ?
        ES_DISPLAY_REQUIRED | ES_SYSTEM_REQUIRED : 0;
    EXECUTION_STATE oldState =
        SetThreadExecutionState(ES_CONTINUOUS | state);

    bool ok = oldState != 0;
    if (ok) {
        d->m_isActive = active;
        Q_EMIT isActiveChanged();
    } else {
        qWarning() << "Setting thread state failed:" << oldState;
    }
}

bool Inhibitor::isActive() const
{
    Q_D(const Inhibitor);
    return d->m_isActive;
}
