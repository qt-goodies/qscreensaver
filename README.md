QScreenSaver
===========

A class to inhibit the screen saver (or screen blanking) for C++/Qt
applications. Multi-platform, aimed at working on any operating system.


How to use it in your application
---------------------------------

QScreenSaver is provided as a static library, whose classes will be embedded
into your compiled application as needed.

One has to have the source tree of QScreenSaver copied into one's project tree;
for those using `git` as their source control system, importing QScreenSaver as
a git submodule might be a good idea.


### Importing the source files

For applications built using [QBS](https://doc.qt.io/qbs/overview.html) this
can be done like this:

    Project {
        Product {
            // ...definition of your product...

            cpp.includePaths: [ screenSaverSources.prefix ]

            Depends { name: "QScreenSaver" }
        }

        references: [
            "thirdparty/qscreensaver/staticlibrary.qbs",
        ]
    }

You are very welcome to create a merge proposal adding support for your
favourite build system.


### Using the parser

QScreenSaver provides the `Inhibitor` class, whose `setActive()` method can be
called with its parameter set to `true` when the screen saver must be
inhibited:

```cpp
#include <ScreenSaver/Inhibitor>

namespace ScreenSaver = it::mardy::ScreenSaver;

class MyApp {
    ...
private:
    ScreenSaver::Inhibitor m_screenSaverInhibitor;
}

void MyApp::inhibitScreenSaver()
{
    m_screenSaverInhibitor.setActive(true);
}

void MyApp::allowScreenSaver()
{
    m_screenSaverInhibitor.setActive(false);
}
```

The `Inhibitor` class also exposes an `active` property (as in `Q_PROPERTY`),
so that the object can be conveniently exposed to QML and handled from there.


### Examples

[MiTubo](http://mardy.it/mitubo) is a video player using QScreenSaver to
inhibit the screen saver while a video is being played. You can find its source
code [here](https://gitlab.com/mardy/mitubo); the `Inhibitor` class is being
exposed to QML in
[`src/types.cpp`](https://gitlab.com/mardy/mitubo/-/blob/master/src/types.cpp)
and the QML `ScreenSaverInhibitor` element is then used in
[`PlayerScreen.qml`](https://gitlab.com/mardy/mitubo/-/blob/master/src/desktop/qml/PlayerScreen.qml).


Licence
-------

QScreenSaver is licensed under the GPL3 licence. [Contact
me](mailto:info@mardy.it) if you need it under a different license.
