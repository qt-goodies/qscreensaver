import qbs 1.0

Project {
    condition: project.buildTests

    Test {
        name: "screensaver-dummy-test"
        files: [
            "../src/ScreenSaver/inhibitor_dummy.cpp",
            "../src/ScreenSaver/inhibitor.h",
            "tst_screensaver_dummy.cpp",
        ]
    }

    Test {
        name: "screensaver-fdo-test"
        files: [
            "../src/ScreenSaver/inhibitor_fdo.cpp",
            "../src/ScreenSaver/inhibitor.h",
            "tst_screensaver_fdo.cpp",
        ]
        Depends { name: 'Qt.dbus' }
        Depends { name: 'libqtdbusmock-1' }
        Depends { name: 'libqtdbustest-1' }
    }

    Test {
        name: "screensaver-windows-test"
        cpp.includePaths: base.concat(["."])
        files: [
            "../src/ScreenSaver/inhibitor_windows.cpp",
            "../src/ScreenSaver/inhibitor.h",
            "tst_screensaver_windows.cpp",
            "windows.h",
        ]
    }

    CoverageClean {
        condition: project.enableCoverage
    }

    CoverageReport {
        condition: project.enableCoverage
        extractPatterns: [ '*/src/ScreenSaver/*.cpp' ]
    }
}
