/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QScreenSaver.
 *
 * QScreenSaver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QScreenSaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScreenSaver/Inhibitor"

#include <QDBusConnection>
#include <QDBusMessage>
#include <QRegularExpression>
#include <QSignalSpy>
#include <QTest>
#include <libqtdbusmock/DBusMock.h>

using namespace it::mardy::ScreenSaver;

class FdoScreenSaverMock
{
    const QString serviceName = "org.freedesktop.ScreenSaver";
    const QString objectPath = "/org/freedesktop/ScreenSaver";
    const QString interfaceName = "org.freedesktop.ScreenSaver";

public:
    FdoScreenSaverMock(QtDBusMock::DBusMock *mock): m_mock(mock) {
        m_mock->registerCustomMock(serviceName,
                                   objectPath,
                                   interfaceName,
                                   QDBusConnection::SessionBus);
    }

    OrgFreedesktopDBusMockInterface &mocked() {
        return m_mock->mockInterface(serviceName,
                                     objectPath,
                                     interfaceName,
                                     QDBusConnection::SessionBus);
    }

    void addMockedMethod(const QString &name,
                         const QString &in_sig,
                         const QString &out_sig,
                         const QString &code)
    {
        return mocked().AddMethod(interfaceName,
                                  name, in_sig, out_sig, code).waitForFinished();
    }

    QList<QtDBusMock::MethodCall> getCalls(const QString &methodName) {
        auto calls = mocked().GetMethodCalls(methodName);
        calls.waitForFinished();
        return calls.value();
    }

    void reset() {
        mocked().ClearCalls();
    }

private:
    QtDBusMock::DBusMock *m_mock;
};

class ScreenSaverTest: public QObject
{
    Q_OBJECT

public:
    ScreenSaverTest();

private Q_SLOTS:
    void init();
    void cleanup();

    void testInhibitNoBackend();
    void testInhibit();
    void testInhibitTwice();

    void testUninhibit();
    void testUninhibitError();
    void testUninhibitTwice();

private:
    QScopedPointer<QtDBusTest::DBusTestRunner> m_dbus;
    QScopedPointer<QtDBusMock::DBusMock> m_mock;
    QScopedPointer<FdoScreenSaverMock> m_fdo;
};

ScreenSaverTest::ScreenSaverTest():
    QObject()
{
    qDBusRegisterMetaType<QtDBusMock::MethodCall>();
    qDBusRegisterMetaType<QList<QtDBusMock::MethodCall>>();
    qDBusRegisterMetaType<QList<QtDBusMock::Method>>();
}

void ScreenSaverTest::init()
{
    if (!m_dbus) {
        m_dbus.reset(new QtDBusTest::DBusTestRunner());
    }
    if (!m_mock) {
        m_mock.reset(new QtDBusMock::DBusMock(*m_dbus.data()));
        // Uncomment for debugging
        //QProcess::startDetached("dbus-monitor", { "--session" });
        m_fdo.reset(new FdoScreenSaverMock(m_mock.data()));
        m_dbus->startServices();
    }
}

void ScreenSaverTest::cleanup()
{
    m_fdo->reset();
}

void ScreenSaverTest::testInhibitNoBackend()
{
    using re = QRegularExpression;
    QTest::ignoreMessage(QtWarningMsg,
                         re("Failed to inhibit screensaver: .*"));
    Inhibitor inhibitor;
    inhibitor.setActive(true);
    QTest::qWait(50);
    QCOMPARE(inhibitor.isActive(), false);
}

void ScreenSaverTest::testInhibit()
{
    m_fdo->addMockedMethod("Inhibit", "ss", "u", "ret = 543");

    Inhibitor inhibitor;

    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QTRY_COMPARE(inhibitor.isActive(), true);
    QCOMPARE(isActiveChanged.count(), 1);
}

void ScreenSaverTest::testInhibitTwice()
{
    QTest::ignoreMessage(QtWarningMsg,
                         "Screensaver inhibitor already active!");
    m_fdo->addMockedMethod("Inhibit", "ss", "u", "ret = 543");

    Inhibitor inhibitor;
    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QVERIFY(isActiveChanged.wait());

    inhibitor.setActive(true);
    QCOMPARE(isActiveChanged.count(), 1);
}

void ScreenSaverTest::testUninhibit()
{
    m_fdo->addMockedMethod("Inhibit", "ss", "u", "ret = 1212");
    m_fdo->addMockedMethod("UnInhibit", "u", "", "");

    Inhibitor inhibitor;

    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QVERIFY(isActiveChanged.wait());
    QCOMPARE(inhibitor.isActive(), true);
    isActiveChanged.clear();

    inhibitor.setActive(false);
    QTRY_COMPARE(inhibitor.isActive(), false);
    QCOMPARE(isActiveChanged.count(), 1);

    const auto calls = m_fdo->getCalls("UnInhibit");
    QCOMPARE(calls.count(), 1);
    const auto args = calls.first().args();
    QCOMPARE(args.count(), 1);
    QCOMPARE(args[0].toUInt(), 1212U);
}

void ScreenSaverTest::testUninhibitError()
{
    using re = QRegularExpression;
    QTest::ignoreMessage(QtWarningMsg,
                         re("Failed to uninhibit screensaver: .*"));

    m_fdo->addMockedMethod("Inhibit", "ss", "u", "ret = 1234");
    m_fdo->addMockedMethod("UnInhibit", "u", "",
                           "raise dbus.exceptions.DBusException('oops', 'bug')");

    Inhibitor inhibitor;

    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QVERIFY(isActiveChanged.wait());
    QCOMPARE(inhibitor.isActive(), true);
    isActiveChanged.clear();

    inhibitor.setActive(false);

    const auto calls = m_fdo->getCalls("UnInhibit");
    QCOMPARE(calls.count(), 1);
    const auto args = calls.first().args();
    QCOMPARE(args.count(), 1);
    QCOMPARE(args[0].toUInt(), 1234U);

    // Iterate the event loop for the D-Bus reply to be processed
    QTest::qWait(10);
    QCOMPARE(isActiveChanged.count(), 0);
    QCOMPARE(inhibitor.isActive(), true);
}

void ScreenSaverTest::testUninhibitTwice()
{
    m_fdo->addMockedMethod("UnInhibit", "u", "", "");

    Inhibitor inhibitor;
    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(false);
    QCOMPARE(m_fdo->getCalls("UnInhibit").count(), 0);
    QCOMPARE(isActiveChanged.count(), 0);
    QCOMPARE(inhibitor.isActive(), false);
}

QTEST_GUILESS_MAIN(ScreenSaverTest)

#include "tst_screensaver_fdo.moc"
