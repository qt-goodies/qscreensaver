/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QScreenSaver.
 *
 * QScreenSaver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QScreenSaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScreenSaver/Inhibitor"

#include <QRegularExpression>
#include <QSignalSpy>
#include <QTest>
#include <functional>
#include <windows.h>

using namespace it::mardy::ScreenSaver;

class WindowsScreenSaverMock
{
public:
    WindowsScreenSaverMock() {
        m_instance = this;
    }
    ~WindowsScreenSaverMock() {
        m_instance = nullptr;
    }
    static WindowsScreenSaverMock *instance() {
        return m_instance;
    }

    using StesCb = std::function<EXECUTION_STATE(EXECUTION_STATE)>;
    void onSetThreadExecutionStateCalled(StesCb callback) {
        m_stesCb = callback;
    }

private:
    friend EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE);
    static WindowsScreenSaverMock *m_instance;
    StesCb m_stesCb;
};

WindowsScreenSaverMock *WindowsScreenSaverMock::m_instance = nullptr;

EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE state)
{
    WindowsScreenSaverMock *mock = WindowsScreenSaverMock::instance();
    if (!mock) return 0;

    return mock->m_stesCb(state);
}

class ScreenSaverTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void init();
    void cleanup();

    void testInhibit();
    void testInhibitError();
    void testInhibitTwice();

    void testUninhibit();
    void testUninhibitError();
    void testUninhibitTwice();

private:
    QScopedPointer<WindowsScreenSaverMock> m_windows;
};

void ScreenSaverTest::init()
{
    m_windows.reset(new WindowsScreenSaverMock());
}

void ScreenSaverTest::cleanup()
{
    m_windows.reset();
}

void ScreenSaverTest::testInhibit()
{
    QList<EXECUTION_STATE> stesCalls;
    m_windows->onSetThreadExecutionStateCalled(
            [&stesCalls](EXECUTION_STATE state) {
        stesCalls.append(state);
        return ES_CONTINUOUS;
    });

    Inhibitor inhibitor;
    QCOMPARE(inhibitor.isActive(), false);

    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QTRY_COMPARE(inhibitor.isActive(), true);
    QCOMPARE(isActiveChanged.count(), 1);
    QCOMPARE(stesCalls.count(), 1);
    QCOMPARE(stesCalls[0], EXECUTION_STATE(
        ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_DISPLAY_REQUIRED));
}

void ScreenSaverTest::testInhibitError()
{
    using re = QRegularExpression;
    QTest::ignoreMessage(QtWarningMsg,
                         re("Setting thread state failed: .*"));

    m_windows->onSetThreadExecutionStateCalled([](EXECUTION_STATE) {
        return 0;
    });

    Inhibitor inhibitor;
    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QCOMPARE(inhibitor.isActive(), false);
    QCOMPARE(isActiveChanged.count(), 0);
}

void ScreenSaverTest::testInhibitTwice()
{
    m_windows->onSetThreadExecutionStateCalled([](EXECUTION_STATE) {
        return ES_CONTINUOUS;
    });

    Inhibitor inhibitor;
    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QTRY_COMPARE(inhibitor.isActive(), true);
    QCOMPARE(isActiveChanged.count(), 1);

    inhibitor.setActive(true);
    QCOMPARE(isActiveChanged.count(), 1);
}

void ScreenSaverTest::testUninhibit()
{
    QList<EXECUTION_STATE> stesCalls;
    m_windows->onSetThreadExecutionStateCalled(
            [&stesCalls](EXECUTION_STATE state) {
        stesCalls.append(state);
        return ES_CONTINUOUS;
    });

    Inhibitor inhibitor;

    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QTRY_COMPARE(inhibitor.isActive(), true);
    QCOMPARE(isActiveChanged.count(), 1);
    isActiveChanged.clear();
    stesCalls.clear();

    inhibitor.setActive(false);
    QTRY_COMPARE(inhibitor.isActive(), false);
    QCOMPARE(isActiveChanged.count(), 1);
    QCOMPARE(stesCalls.count(), 1);
    QCOMPARE(stesCalls[0], ES_CONTINUOUS);
}

void ScreenSaverTest::testUninhibitError()
{
    using re = QRegularExpression;
    QTest::ignoreMessage(QtWarningMsg,
                         re("Setting thread state failed: .*"));

    int stesCallCount = 0;
    m_windows->onSetThreadExecutionStateCalled(
            [&stesCallCount](EXECUTION_STATE) {
        stesCallCount++;
        return stesCallCount == 1 ? ES_CONTINUOUS : 0;
    });

    Inhibitor inhibitor;

    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QTRY_COMPARE(inhibitor.isActive(), true);
    QCOMPARE(isActiveChanged.count(), 1);
    isActiveChanged.clear();

    inhibitor.setActive(false);
    QCOMPARE(inhibitor.isActive(), true);
    QCOMPARE(isActiveChanged.count(), 0);
    QCOMPARE(stesCallCount, 2);
}

void ScreenSaverTest::testUninhibitTwice()
{
    int stesCallCount = 0;
    m_windows->onSetThreadExecutionStateCalled(
            [&stesCallCount](EXECUTION_STATE) {
        stesCallCount++;
        return ES_CONTINUOUS;
    });

    Inhibitor inhibitor;
    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(false);
    QCOMPARE(stesCallCount, 0);
    QCOMPARE(isActiveChanged.count(), 0);
    QCOMPARE(inhibitor.isActive(), false);
}

QTEST_GUILESS_MAIN(ScreenSaverTest)

#include "tst_screensaver_windows.moc"
