/*
 * Copyright (C) 2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of QScreenSaver.
 *
 * QScreenSaver is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QScreenSaver is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QHtmlParser.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScreenSaver/Inhibitor"

#include <QSignalSpy>
#include <QTest>

using namespace it::mardy::ScreenSaver;

class ScreenSaverTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testInhibit();
    void testUninhibit();
};

void ScreenSaverTest::testInhibit()
{
    QTest::ignoreMessage(QtWarningMsg,
        "ScreenSaver inhibitor not implemented for this platform");

    Inhibitor inhibitor;
    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(true);
    QCOMPARE(isActiveChanged.count(), 0);
    QCOMPARE(inhibitor.isActive(), false);
}

void ScreenSaverTest::testUninhibit()
{
    QTest::ignoreMessage(QtWarningMsg,
        "ScreenSaver inhibitor not implemented for this platform");

    Inhibitor inhibitor;
    QSignalSpy isActiveChanged(&inhibitor, &Inhibitor::isActiveChanged);
    inhibitor.setActive(false);
    QCOMPARE(isActiveChanged.count(), 0);
    QCOMPARE(inhibitor.isActive(), false);
}

QTEST_GUILESS_MAIN(ScreenSaverTest)

#include "tst_screensaver_dummy.moc"
